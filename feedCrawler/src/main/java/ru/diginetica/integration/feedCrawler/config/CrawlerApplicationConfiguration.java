package ru.diginetica.integration.feedCrawler.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.SimpleProductRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.custom.AptekaMedProductRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.custom.CDWProductRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.custom.MasysProductRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.custom.SportmasterProductRetriever;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.ShareRatios;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.SmartLabSharesInfoRetriever;
import ru.diginetica.integration.feedCrawler.output.EntityAsStringToFilePostProcessor;
import ru.diginetica.integration.feedCrawler.output.PostProcessor;
import ru.diginetica.integration.feedCrawler.output.shares.SharesAnalyzerPostProcessor;
import ru.diginetica.integration.feedCrawler.product.Product;

@Configuration
public class CrawlerApplicationConfiguration {

  @ConditionalOnMissingBean(EntityRetriever.class)
  @Bean
  public EntityRetriever<Product> simpleEntityRetriever(ProductQueryConfig productQueryConfig) {
    return new SimpleProductRetriever(productQueryConfig);
  }

  @Bean
  @Primary
  @Profile("cdw")
  public EntityRetriever<Product> cdwEntityRetriever(ProductQueryConfig productQueryConfig) {
    return new CDWProductRetriever(productQueryConfig);
  }

  @Bean
  @Primary
  @Profile("masys")
  public EntityRetriever<Product> masysEntityRetriever(ProductQueryConfig productQueryConfig) {
    return new MasysProductRetriever(productQueryConfig);
  }

  @Bean
  @Primary
  @Profile("sportmaster")
  public EntityRetriever<Product> sportmasterEntityRetriever() {
    return new SportmasterProductRetriever();
  }

  @Bean
  @Primary
  @Profile("apteka-med")
  public EntityRetriever<Product> aptekamedEntityRetriever(ProductQueryConfig productQueryConfig) {
    return new AptekaMedProductRetriever(productQueryConfig);
  }

  @Bean
  @Primary
  @Profile("smartlab")
  public EntityRetriever<ShareRatios> smartLabShareRatios(RouteConfig routeConfig) {
    return new SmartLabSharesInfoRetriever(routeConfig.getShouldProcessUrlPatterns().get(0));
  }

//  @Bean
//  @Profile("smartlab")
//  public PostProcessor entityAsStringToFilePostProcessor(SharesAnalyser sharesAnalyser) {
//    return new SharesRatioPostProcessor(sharesAnalyser);
//  }

  @Bean
  @Primary
  @Profile("smartlab")
  public PostProcessor entityAsStringToFilePostProcessor() {
    return new SharesAnalyzerPostProcessor();
  }

  @ConditionalOnMissingBean(PostProcessor.class)
  @Bean
  public PostProcessor postProcessor() {
    return new EntityAsStringToFilePostProcessor<>();
  }

}
