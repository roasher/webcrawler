package ru.diginetica.integration.feedCrawler.task;

import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.diginetica.integration.feedCrawler.config.CrawlConfig;

@Component
@Slf4j
public class TaskProvider implements InitializingBean {

  @Autowired
  private CrawlConfig config;

  private ConcurrentLinkedQueue<Task> tasksQueue = new ConcurrentLinkedQueue<>();
  // Containing all urls that has been crawled + urls form taskQueue
  private Set<URL> urls = ConcurrentHashMap.newKeySet();
  private Set<Task> unresolvedTasks = ConcurrentHashMap.newKeySet();
  private AtomicBoolean workFinished = new AtomicBoolean();

  private void addNewTask(URL parentUrl, URL url, int crawlDepth) {
    // we need to be sure that queue doesn't already have task to parse same url
    if (!urls.contains(url)) {
      Task task = new Task(parentUrl, url, crawlDepth);
      urls.add(url);
      tasksQueue.add(task);
      log.debug("Url was added {}", url);
    }
  }

  public synchronized void claimFinish(Task task, List<URL> urls) {
    if (task.getCrowlDepth() != 1) {
      for (URL url : urls) {
        addNewTask(task.getUrlToCrawl(), url, task.getCrowlDepth() - 1);
      }
    }
    unresolvedTasks.remove(task);
    if (tasksQueue.isEmpty() && unresolvedTasks.isEmpty()) {
      finishWork();
    }
  }

  public synchronized Task pollTask() {
    Task task = tasksQueue.poll();
    if (task != null) {
      unresolvedTasks.add(task);
      log.debug("Task was polled {}", task);
    }
    return task;
  }

  public boolean isWorkFinished() {
    return workFinished.get();
  }

  public void finishWork() {
    workFinished.set(true);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
      // initWithFirstTask
      URL firstUrl = new URL(config.getUrl());
      Task firstTask = new Task(null, firstUrl, config.getCrawlDepth());
      tasksQueue.add(firstTask);
      urls.add(firstUrl);
  }

}
