package ru.diginetica.integration.feedCrawler.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "connection")
public class ConnectionConfig {

  private Integer connectionTimeout;
  private Integer readTimeout;

}
