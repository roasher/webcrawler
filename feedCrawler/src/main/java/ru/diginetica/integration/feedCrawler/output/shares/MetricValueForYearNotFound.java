package ru.diginetica.integration.feedCrawler.output.shares;

public class MetricValueForYearNotFound extends RuntimeException {
  public MetricValueForYearNotFound(String s) {
    super(s);
  }
}
