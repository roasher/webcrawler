package ru.diginetica.integration.feedCrawler.crawler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import ru.diginetica.integration.feedCrawler.config.CrawlConfig;
import ru.diginetica.integration.feedCrawler.config.RouteConfig;
import ru.diginetica.integration.feedCrawler.task.Task;
import ru.diginetica.integration.feedCrawler.task.TaskProvider;
import ru.diginetica.integration.feedCrawler.utils.Utils;

/**
 * Created by pyurkin on 20.05.16.
 */
@Slf4j
@RequiredArgsConstructor
public abstract class Crawler implements Runnable {

  protected final CrawlConfig crawlConfig;
  protected final RouteConfig routeConfig;
  private final TaskProvider taskProvider;
  private final WebDriver webDriver;

  public abstract boolean shouldVisit(URL parentUrl, URL url);
  public abstract boolean shouldProcess(URL url);
  public abstract void process(Document document, URL processingUrl);

  public static final Pattern IMAGE_PATTERN = Pattern.compile(".*\\.(bmp|gif|jpg|png)$");

  @Override
  public void run() {
    while (!taskProvider.isWorkFinished()) {
      Task task = taskProvider.pollTask();
      if (task != null) {
        List<URL> urls = new ArrayList<>();
        URL url = task.getUrlToCrawl();
        // if parent url is null this means we are crawling basic site
        if (task.getParentUrl() == null || shouldVisitCommon(task.getParentUrl(), url)) {
          log.debug("visited: {}", url.toString());
          getParsedUrl(url).ifPresent(document -> {
            if (shouldProcess(url)) {
              process(document, url);
            }
            Set<URL> linksFromPage = getLinksFromDocument(document);
            urls.addAll(linksFromPage);
          });
        }
        taskProvider.claimFinish(task, urls);
      }
    }
  }

  private boolean shouldVisitCommon(URL parentUrl, URL urlToVisit) {
    if (isImg(urlToVisit)) {
      log.debug("{} is image, skipping", urlToVisit);
      return false;
    }
    if (!urlToVisit.getProtocol().equals("http") && !urlToVisit.getProtocol().equals("https")) {
      log.debug("Protocol {} is not http or https, skipping", urlToVisit.getProtocol());
      return false;
    }
    return shouldVisit(parentUrl, urlToVisit);
  }

  private boolean isImg(URL urlToVisit) {
    String href = urlToVisit.toString().toLowerCase();
    if (IMAGE_PATTERN.matcher(href).matches()) {
      return true;
    }
    return false;
  }


  private Set<URL> getLinksFromDocument(Document document) {
    Set<URL> urls = new HashSet<>();
    Elements linkElements = document.select("a[href]");
    for (Element linkElement : linkElements) {
      try {
        String attr = linkElement.attr("abs:href");
        if (!StringUtils.isEmpty(attr)) {
          urls.add(new URL(attr));
        }
      } catch (MalformedURLException e) {
        e.printStackTrace();
      }
    }
    return urls;
  }

  private Optional<Document> getParsedUrl(URL url) {
    Optional<String> html = getHtmlWithRetries(url, crawlConfig.getNumberOfLoadTries());
    return html.isPresent() ? Optional.of(Jsoup.parse(html.get(), url.toString())) : Optional.empty();
  }

  private Optional<String> getHtmlWithRetries(URL url, int numberOfTryies) {
    Optional<String> html;
    int currentTryNumber = 0;
    while (currentTryNumber < numberOfTryies) {
      try {
        html = getHtml(url, crawlConfig.getUseJs() && shouldProcess(url));
        return html;
      } catch (IOException e) {
        log.info("IOException occurs. Retrying to load url: {}. Try number: {}", url.toString(), currentTryNumber);
        currentTryNumber++;
      }
    }
    return Optional.empty();
  }

  private Optional<String> getHtml(URL url, boolean useJs) throws IOException {
    delay(crawlConfig.getPolitenessDelay());
    webDriver.get(String.valueOf(url));
    String pageSource = Utils.getPageSource(webDriver);
    if (invalid(pageSource)) {
      return Optional.empty();
    }
    return Optional.of(pageSource);
  }

  protected boolean invalid(String pageSource) {
    // TODO implementation
    return false;
  }

  private void delay(Integer politenessDelay) {
    try {
      Thread.sleep(politenessDelay);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
