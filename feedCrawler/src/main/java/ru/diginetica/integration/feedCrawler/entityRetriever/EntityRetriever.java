package ru.diginetica.integration.feedCrawler.entityRetriever;

/**
 * Created by pyurkin on 01.06.16.
 */

import java.net.URL;
import java.util.Optional;
import org.jsoup.nodes.Document;


public interface EntityRetriever<E extends Entity> {

  Optional<E> getEntityFromPage(Document document, URL url);

}
