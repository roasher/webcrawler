package ru.diginetica.integration.feedCrawler.entityRetriever.smartlab;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Metric implements Serializable {

  private String name;
  private String dimension;

}
