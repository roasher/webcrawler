package ru.diginetica.integration.feedCrawler.output;

import static ru.diginetica.integration.feedCrawler.utils.Utils.listToStringRRStyle;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import ru.diginetica.integration.feedCrawler.product.Product;

@RequiredArgsConstructor
public class EntityAsStringToFilePostProcessor<E> implements PostProcessor<E> {

  @Value("${output}")
  private String fileName;

  @Override
  public void process(Set<E> entities) {
    String filePath = "feedCrawler/crawl_data/" + fileName;
    try (PrintWriter printWriter = new PrintWriter(new FileWriter(filePath))) {
      // Feed
      entities.forEach(product -> printWriter.println(product.toString()));
      printWriter.println(entities.size());
      printWriter.println();
      // Category tree
//      CategoryTree(entites, printWriter);
      printWriter.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void categoryTree(Set<Product> entites, PrintWriter printWriter) {
    Map<String, List<Product>> breadcrumbsMap = entites.stream()
        .collect(Collectors.groupingBy(product -> listToStringRRStyle(product.getBreadcrumbs())));
    breadcrumbsMap.keySet().stream().sorted().forEach(strings -> {
      printWriter.println((strings) + "-" + breadcrumbsMap.get(strings).size());
    });
  }

}
