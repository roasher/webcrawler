package ru.diginetica.integration.feedCrawler.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "product")
public class ProductQueryConfig {

  private String id;
  private String name;
  private String price;
  private String brandQuery;
  private String descriptionQuery;
  private String breadCrumbsQuery;

}
