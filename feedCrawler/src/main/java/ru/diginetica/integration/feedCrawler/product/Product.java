package ru.diginetica.integration.feedCrawler.product;

import java.net.URL;
import java.util.Date;
import java.util.List;
import lombok.Data;
import ru.diginetica.integration.feedCrawler.entityRetriever.Entity;
/**
 * Created by pyurkin on 20.05.16.
 */
@Data
public class Product implements Entity {

  private String id;
  private String name;
  private String brand;
  private String price;
  private String description;
  private List<String> breadcrumbs;
  private URL url;
  // time when crawled(created)
  private Date date = new Date();

  public Product(String id, String name, String brand, String price, String description,
                 List<String> breadcrumbs, URL url) {
    this.id = id;
    this.name = name;
    this.brand = brand;
    this.price = price;
    this.description = description;
    this.breadcrumbs = breadcrumbs;
    this.url = url;
  }
}
