package ru.diginetica.integration.feedCrawler.product;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;

/**
 * Created by pyurkin on 30.05.16.
 */
@Component
public class EntityDAO<T> {

  private Set<T> entities = ConcurrentHashMap.newKeySet();

  public Set<T> getEntites() {
    return entities;
  }

  public void persistProduct(T product) {
    entities.add(product);
  }
}
