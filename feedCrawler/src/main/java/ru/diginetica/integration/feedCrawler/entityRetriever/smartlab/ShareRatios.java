package ru.diginetica.integration.feedCrawler.entityRetriever.smartlab;

import java.util.Map;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.diginetica.integration.feedCrawler.entityRetriever.Entity;

@Data
@AllArgsConstructor
public class ShareRatios implements Entity {

  private String id;
  private String name;

  private Map<Metric, Map<String, Optional<Double>>> ratios;

}
