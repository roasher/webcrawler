package ru.diginetica.integration.feedCrawler.output;

import java.util.Set;

public interface PostProcessor<E> {

  void process(Set<E> entities);

}
