package ru.diginetica.integration.feedCrawler.config;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import java.io.File;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class WebDriverConfig {

  @Value("${webdriver.chrome.driver:}")
  private String chromeBinary;

  @Bean
  @ConditionalOnProperty(prefix = "crawler", name = "useJs", havingValue = "true")
  @Scope("prototype")
  public WebDriver chromeWebDriver() {
    if (chromeBinary != null) {
      System.setProperty("webdriver.chrome.driver", chromeBinary);
    }
    ChromeOptions options = new ChromeOptions();
    options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors");
    options.setBinary(new File(chromeBinary));
    return new ChromeDriver(options);
  }

  @Bean
  @ConditionalOnProperty(prefix = "crawler", name = "useJs", havingValue = "false")
  @Scope("prototype")
  public WebDriver htmlUnitWebDriver() {
    return new HtmlUnitDriver(BrowserVersion.CHROME, false) {
      @Override
      protected WebClient modifyWebClient(WebClient client) {
        final WebClient webClient = super.modifyWebClient(client);
        // you might customize the client here
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setDownloadImages(false);
        webClient.getOptions().setJavaScriptEnabled(false);
        return webClient;
      }
    };
  }

}
