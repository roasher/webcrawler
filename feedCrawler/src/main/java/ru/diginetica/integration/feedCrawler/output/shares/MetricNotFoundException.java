package ru.diginetica.integration.feedCrawler.output.shares;

public class MetricNotFoundException extends RuntimeException {
  public MetricNotFoundException(String s) {
    super(s);
  }
}
