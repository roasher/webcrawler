package ru.diginetica.integration.feedCrawler.task;

import java.net.URL;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Data;

/**
 * Created by pyurkin on 20.05.16.
 */
@Data
public class Task {

  private static AtomicLong counter = new AtomicLong(0);

  private long id;
  private URL parentUrl;
  private URL urlToCrawl;
  private int crowlDepth;

  public Task(URL parentUrl, URL urlToCrawl, int crowlDepth) {
    this.id = counter.addAndGet(1);
    this.parentUrl = parentUrl;
    this.urlToCrawl = urlToCrawl;
    this.crowlDepth = crowlDepth;
  }

}
