package ru.diginetica.integration.feedCrawler.config;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.Data;
import lombok.Getter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "routes")
@Data
public class RouteConfig {

  private List<String> shouldVisit;
  private List<String> shouldProcess;

  @Getter
  private List<Pair<Pattern, Pattern>> shouldVisitRoutePatterns;
  @Getter
  private List<Pattern> shouldProcessUrlPatterns;

  @PostConstruct
  public void setShouldVisit() {
    if (shouldVisit.size() % 2 != 0) {
      throw new IllegalArgumentException();
    }
    List<Pair<Pattern, Pattern>> out = new ArrayList<>();
    for (int pairNumber = 0; pairNumber < shouldVisit.size(); pairNumber = pairNumber + 2) {
      out.add(new ImmutablePair<>(Pattern.compile(shouldVisit.get(pairNumber)),
          Pattern.compile(shouldVisit.get(pairNumber + 1))));
    }
    shouldVisitRoutePatterns = out;
  }

  @PostConstruct
  public void setShouldProcess() {
    shouldProcessUrlPatterns = shouldProcess.stream()
        .map(Pattern::compile)
        .collect(Collectors.toList());
  }

}
