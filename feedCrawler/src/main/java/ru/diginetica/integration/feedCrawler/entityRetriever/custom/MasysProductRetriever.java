package ru.diginetica.integration.feedCrawler.entityRetriever.custom;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.config.ProductQueryConfig;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.product.Product;

/**
 * Created by pyurkin on 06.06.16.
 */
@RequiredArgsConstructor
public class MasysProductRetriever implements EntityRetriever<Product> {

  private final ProductQueryConfig productQueryConfig;
  private Pattern pattern = Pattern.compile("ID=(\\d+)");

  @Override
  public Optional<Product> getEntityFromPage(Document document, URL url) {
    String id;
    Matcher matcher = pattern.matcher(url.toString());
    if (matcher.find()) {
      id = matcher.group().substring(3);
    } else {
      return Optional.empty();
    }

    String name = document.select(productQueryConfig.getName()).text();
    String price = document.select(productQueryConfig.getPrice()).text();
    String brand = name.split(" ")[0];
    String description = document.select(productQueryConfig.getDescriptionQuery()).text();

    List<String> breadcrumbs = new ArrayList<>();
    Elements select = document.select(productQueryConfig.getBreadCrumbsQuery());
    select.forEach(element -> breadcrumbs.add(element.text()));

    return Optional.of(new Product(id, name, brand, price, description, breadcrumbs, url));
  }

}
