package ru.diginetica.integration.feedCrawler.entityRetriever.custom;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.config.ProductQueryConfig;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.product.Product;

/**
 * Created by pyurkin on 03.06.16.
 */
@RequiredArgsConstructor
public class CDWProductRetriever implements EntityRetriever<Product> {

  private final ProductQueryConfig productQueryConfig;

  @Override
  public Optional<Product> getEntityFromPage(Document document, URL url) {
    String id = null;
    String idContainingString = document.select("#primaryProductPartNumbers .part-number").get(1).text();
    Matcher matcher = Pattern.compile("\\d+").matcher(idContainingString);
    if (matcher.find()) {
      id = matcher.group();
    }
    String name = document.select(productQueryConfig.getName()).text();
    String price = document.select(productQueryConfig.getPrice()).text();
    String brand = document.select(productQueryConfig.getBrandQuery()).text();
    String description = document.select(productQueryConfig.getDescriptionQuery()).text();

    List<String> breadcrumbs = new ArrayList<>();
    Elements select = document.select(productQueryConfig.getBreadCrumbsQuery());
    select.forEach(element -> breadcrumbs.add(element.text()));

    return Optional.of(new Product(id, name, brand, price, description, breadcrumbs, url));
  }

}
