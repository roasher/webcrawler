package ru.diginetica.integration.feedCrawler;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.diginetica.integration.feedCrawler.crawler.ConfigurableCrawler;
import ru.diginetica.integration.feedCrawler.output.PostProcessor;
import ru.diginetica.integration.feedCrawler.product.EntityDAO;
import ru.diginetica.integration.feedCrawler.task.TaskProvider;

/**
 * Created by pyurkin on 17.05.16.
 */
@SpringBootApplication
public class FeedCrawlerApplication {

  public static void main(String... args) throws InterruptedException {

    ApplicationContext context = SpringApplication.run(FeedCrawlerApplication.class);

    int threadNumbers = Integer.parseInt(Objects.requireNonNull(context.getEnvironment().getProperty("crawler.workerNumber")));
    ExecutorService executorService = Executors.newFixedThreadPool(threadNumbers);
    for (int currentCrawlerNumber = 0; currentCrawlerNumber < threadNumbers; currentCrawlerNumber++) {
      executorService.execute(context.getBean(ConfigurableCrawler.class));
    }

    executorService.shutdown();
    executorService.awaitTermination(10, TimeUnit.MINUTES);
    context.getBean(TaskProvider.class).finishWork();

    context.getBean(PostProcessor.class).process(context.getBean(EntityDAO.class).getEntites());

  }

}
