package ru.diginetica.integration.feedCrawler.entityRetriever.custom;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.config.ProductQueryConfig;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.product.Product;

/**
 * Created by pyurkin on 10.06.16.
 */
@RequiredArgsConstructor
public class AptekaMedProductRetriever implements EntityRetriever<Product> {

  private final ProductQueryConfig productQueryConfig;
  private final String ID_PATTERN = "арт";

  @Override
  public Optional<Product> getEntityFromPage(Document document, URL url) {

    Elements possibleIds = document.select("div#offer_full p");
    Optional<String> posId = possibleIds.stream().map(Element::text).filter(element -> element.contains(ID_PATTERN))
      .findFirst();

    String id = StringUtils.remove(posId.orElse(""), "(товара нет в наличии)");

    Optional<String> posName = possibleIds.stream().map(Element::text)
      .filter(element -> !StringUtils.isEmpty(element) && !element.contains(ID_PATTERN))
      .findFirst();

    String name = posName.orElse("");
    String price = document.select(productQueryConfig.getPrice()).text();
    String brand = null;
    String description = document.select(productQueryConfig.getDescriptionQuery()).text();

    List<String> breadcrumbs = new ArrayList<>();
    Elements select = document.select(productQueryConfig.getBreadCrumbsQuery());
    select.forEach(element -> breadcrumbs.add(element.text()));

    return Optional.of(new Product(id, name, brand, price, description, breadcrumbs, url));
  }
}
