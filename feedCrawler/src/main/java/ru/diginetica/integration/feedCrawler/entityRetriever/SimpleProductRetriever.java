package ru.diginetica.integration.feedCrawler.entityRetriever;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.config.ProductQueryConfig;
import ru.diginetica.integration.feedCrawler.product.Product;

/**
 * Created by pyurkin on 01.06.16.
 */
@RequiredArgsConstructor
public class SimpleProductRetriever implements EntityRetriever<Product> {

  private final ProductQueryConfig productQueryConfig;

  @Override
  public Optional<Product> getEntityFromPage(Document document, URL url) {
    String id = document.select(productQueryConfig.getId()).text();
    String name = document.select(productQueryConfig.getName()).text();
    String price = document.select(productQueryConfig.getPrice()).text();
    String brand = document.select(productQueryConfig.getBrandQuery()).text();
    String description = document.select(productQueryConfig.getDescriptionQuery()).text();

    List<String> breadcrumbs = new ArrayList<>();
    Elements select = document.select(productQueryConfig.getBreadCrumbsQuery());
    select.forEach(element -> breadcrumbs.add(element.text()));

    return Optional.of(new Product(id, name, brand, price, description, breadcrumbs, url));
  }
}
