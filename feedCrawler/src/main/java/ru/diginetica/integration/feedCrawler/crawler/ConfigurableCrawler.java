package ru.diginetica.integration.feedCrawler.crawler;

import java.net.URL;
import java.util.Optional;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.diginetica.integration.feedCrawler.config.CrawlConfig;
import ru.diginetica.integration.feedCrawler.config.RouteConfig;
import ru.diginetica.integration.feedCrawler.entityRetriever.Entity;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.product.EntityDAO;
import ru.diginetica.integration.feedCrawler.task.TaskProvider;

@Component
@Scope("prototype")
@Slf4j
public class ConfigurableCrawler<T extends Entity> extends Crawler {

  private final EntityRetriever<T> entityRetriever;
  protected final EntityDAO<T> entityDAO;

  public ConfigurableCrawler(CrawlConfig config,
                             RouteConfig routeConfig,
                             TaskProvider taskProvider,
                             WebDriver webDriver,
                             EntityRetriever<T> entityRetriever,
                             EntityDAO<T> entityDAO) {
    super(config, routeConfig, taskProvider, webDriver);
    this.entityRetriever = entityRetriever;
    this.entityDAO = entityDAO;
  }

  @Override
  public boolean shouldVisit(URL parentUrl, URL url) {
    log.debug("Calculating should visit {} from it's parent {}", url, parentUrl);
    for (Pair<Pattern, Pattern> pair : routeConfig.getShouldVisitRoutePatterns()) {
      Pattern parentPattern = pair.getLeft();
      Pattern urlPattern = pair.getRight();
      boolean parentPatternMatch = parentPattern.matcher(parentUrl.toString()).matches();
      boolean urlPatternMatch = urlPattern.matcher(url.toString()).matches();
      if (parentPatternMatch && urlPatternMatch) {
        log.debug("{} matches {}, and it's parent {} matches {}. We should visit this.", url, urlPattern, parentUrl, parentPattern);
        return true;
      }
    }
    log.debug("Transition urls not matched for any patters. Considering NOT to visit url {}", url);
    return false;
  }

  @Override
  public boolean shouldProcess(URL url) {
    log.debug("Considering whether we should to process {}", url);
    for (Pattern pattern : routeConfig.getShouldProcessUrlPatterns()) {
      if (pattern.matcher(url.toString()).matches()) {
        log.debug("{} matches {}. Should process.", url, pattern);
        return true;
      }
    }
    log.debug("{} doesn't match any patterns. Disided NOT to process", url);
    return false;
  }

  @Override
  public void process(Document document, URL processingUrl) {
    log.debug("processed: {}", processingUrl.toString());
    Optional<T> product = entityRetriever.getEntityFromPage(document, processingUrl);
    product.ifPresent(product1 -> {
      if (!StringUtils.isEmpty(product1.getId())) {
        log.info("Product found: {}", product.toString());
        entityDAO.persistProduct(product1);
      }
    });
  }


}
