package ru.diginetica.integration.feedCrawler.entityRetriever.smartlab;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;

@RequiredArgsConstructor
public class SmartLabSharesInfoRetriever implements EntityRetriever<ShareRatios> {

  public static final String LTM = "LTM";
  private final Pattern urlPattern;

  @Override
  public Optional<ShareRatios> getEntityFromPage(Document document, URL url) {
    Elements financial = document.select(".financials");

    Map<String, Integer> yearToColumnNumbers = new HashMap<>();
    Elements yearHeaders = financial.select(".header_row").select("td+ td");
    for (int column = 0; column < yearHeaders.size(); column++) {
      String text = yearHeaders.get(column).text();
      if (!StringUtils.isEmpty(text) && (text.contains("LTM") || NumberUtils.isCreatable(text))) {
        yearToColumnNumbers.put(text.contains("LTM") ? LTM : text, column);
      }
    }

    Map<Metric, Map<String, Optional<Double>>> metricMap = financial
        .select("tr[field]")
        .select("tr[field!=date]")
        .select("tr[field!=currency]")
        .select("tr[field!=report_url]")
        .select("tr[field!=year_report_url]")
        .select("tr[field!=presentation_url]")
        .select("tr[field!=company_report_separator]")
        .stream()
        .collect(Collectors.toMap(this::getMetric, element ->
            yearToColumnNumbers.entrySet()
                .stream()
                .collect(Collectors
                    .toMap(Map.Entry::getKey,
                        yearToColumnNumber -> {
                          String text =
                              element.select("td+ td").get(yearToColumnNumber.getValue()).text()
                                  .trim()
                                  .replace(" ", "")
                                  .replace("%", "");
                          return !StringUtils.isEmpty(text) ? Optional.of(Double.valueOf(text)) : Optional.empty();
                        }))));

    return Optional.of(new ShareRatios(getId(url), getName(document), metricMap));
  }

  private String getName(Document document) {
    return document.select("h1").text();
  }

  private String getId(URL url) {
    Matcher matcher = urlPattern.matcher(url.toString());
    matcher.find();
    return matcher.group(1);
  }

  private Metric getMetric(Element element) {
    Elements firstCell = element.child(0).children();
    return new Metric(firstCell.get(0).text(), firstCell.size() > 1 ? firstCell.get(1).text() : "");
  }

}
