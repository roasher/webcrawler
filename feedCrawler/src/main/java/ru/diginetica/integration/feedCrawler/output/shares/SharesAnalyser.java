package ru.diginetica.integration.feedCrawler.output.shares;

import static java.util.stream.Collectors.toMap;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.chosenMetrics;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.Metric;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.ShareRatios;

@Component
public class SharesAnalyser {

  public void analyze(Set<ShareRatios> entities) {
    Map<Metric, HashSet<String>> metricByCompanies = entities.stream()
        .flatMap(shareRatios -> shareRatios.getRatios().keySet().stream()
            .map(metric -> Pair.of(metric, shareRatios.getId())))
        .collect(toMap(Pair::getLeft, stringStringPair -> {
          HashSet<String> setOfCompanies = new HashSet<>();
          setOfCompanies.add(stringStringPair.getRight());
          return setOfCompanies;
        }, (hashSet, hashSet2) -> {
          hashSet.addAll(hashSet2);
          return hashSet;
        }));
    metricByCompanies.entrySet().stream()
        .sorted(Comparator.comparingInt(o -> o.getValue().size()))
        .forEach(stringHashSetEntry -> System.out.println(
            stringHashSetEntry.getKey() + "|" + stringHashSetEntry.getValue().size() + "|" + stringHashSetEntry
                .getValue().toString()));

    Optional<HashSet<String>> reduce = chosenMetrics.stream()
        .peek(System.out::println)
        .map(metricByCompanies::get)
        .reduce((strings, strings2) -> {
          strings.retainAll(strings2);
          return strings;
        });

    System.out.println();
    System.out.println(reduce.get());
    System.out.println(reduce.get().size());
  }
}
