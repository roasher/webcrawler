package ru.diginetica.integration.feedCrawler.entityRetriever.custom;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.diginetica.integration.feedCrawler.entityRetriever.EntityRetriever;
import ru.diginetica.integration.feedCrawler.product.Product;

/**
 * Created by pyurkin on 03.06.16.
 */
public class SportmasterProductRetriever implements EntityRetriever<Product> {

  @Override
  public Optional<Product> getEntityFromPage(Document document, URL url) {
    String id = url.toString().split("/")[4];
    String name = document.select(".sm-goods_main_details h1").text();
    String price = document.select(".sm-goods_main_details_prices_actual-price.rouble").text();
    String brand = null;
    Elements characteristixs = document.select(".sm-goods_tabs_characteristix tr td");
    for (int chNumber = 0; chNumber < characteristixs.size(); chNumber++) {
      if (characteristixs.get(chNumber).text().equals("Производитель") && chNumber + 1 != characteristixs.size()) {
        brand = characteristixs.get(chNumber + 1).text();
      }
    }
    String description = document.select(".sm-goods__description-text").text();

    List<String> breadcrumbs = new ArrayList<>();
    Elements select = document.select("a.sm-breadcrumbs__sub");
    select.forEach(element -> breadcrumbs.add(element.text()));

    return Optional.of(new Product(id, name, brand, price, description, breadcrumbs, url));
  }

}
