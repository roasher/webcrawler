package ru.diginetica.integration.feedCrawler.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "crawler")
@Data
public class CrawlConfig {

  private String url;
  private Integer crawlDepth;
  private Boolean useJs;
  private Integer politenessDelay;
  private Integer numberOfLoadTries;

}
