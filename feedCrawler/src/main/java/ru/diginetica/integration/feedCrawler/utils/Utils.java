package ru.diginetica.integration.feedCrawler.utils;

import java.util.List;
import org.openqa.selenium.WebDriver;

/**
 * Created by pyurkin on 06.06.16.
 */
public class Utils {

  public static String getPageSource(WebDriver webDriver) {
    return webDriver.getPageSource();
  }

  public static <T extends Object> String listToStringRRStyle(List<T> objects) {
    StringBuilder stringBuilder = new StringBuilder();
    objects.forEach(object -> stringBuilder.append(object).append("|"));
    return stringBuilder.toString();
  }
}
