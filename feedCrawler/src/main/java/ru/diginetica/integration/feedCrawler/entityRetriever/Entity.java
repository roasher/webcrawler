package ru.diginetica.integration.feedCrawler.entityRetriever;

public interface Entity {

  String getId();

}
