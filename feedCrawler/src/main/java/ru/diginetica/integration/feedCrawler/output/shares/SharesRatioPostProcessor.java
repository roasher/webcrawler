package ru.diginetica.integration.feedCrawler.output.shares;

import static ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.SmartLabSharesInfoRetriever.LTM;

import com.google.common.annotations.VisibleForTesting;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.Metric;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.ShareRatios;
import ru.diginetica.integration.feedCrawler.output.PostProcessor;

@RequiredArgsConstructor
@Slf4j
public class SharesRatioPostProcessor implements PostProcessor<ShareRatios> {

  public static final int YEAR_NUMBER = 2;
  private final SharesAnalyser sharesAnalyser;

  public static final Metric ASSETS = new Metric("Активы", "млрд руб");
  public static final Metric NET_ASSETS = new Metric("Чистые активы", "млрд руб");
  public static final Metric DIVIDEND = new Metric("Дивиденд", "руб/акцию");
  public static final Metric DIVIDEND_EARNING = new Metric("Див доход, ао", "%");
  public static final Metric SHARE_PRICE = new Metric("Цена акции ао", "руб");
  public static final Metric DEPT = new Metric("Долг", "млрд руб");
  public static final Metric NET_DEPT = new Metric("Чистый долг", "млрд руб");
  public static final Metric COMMON_SHARES_NUMBER = new Metric("Число акций ао", "млн");
  public static final Metric PREMIUM_SHARES_NUMBER = new Metric("Число акций ап", "млн");
  public static final Metric CASH_AND_EQUIVALENTS = new Metric("Наличность", "млрд руб");
  public static final Metric CAPITALIZATION = new Metric("Капитализация", "млрд руб");
  public static final Metric PRICE_PER_EARNINGS = new Metric("P/E", "");
  public static final Metric PRICE_PER_BALANCE_VALUE = new Metric("P/BV", "");
  public static final Metric PRICE_PER_BOOK = new Metric("P/B", "");
  public static final Metric NET_INCOME = new Metric("Чистая прибыль", "млрд руб");

  public static final List<Metric> chosenMetrics = new ArrayList<>() {{
    add(new Metric("EV", "млрд руб"));
    add(CAPITALIZATION);
    add(COMMON_SHARES_NUMBER);
    add(NET_INCOME);
    add(new Metric("EPS", "руб"));
    add(SHARE_PRICE);
//    add(PRICE_PER_EARNINGS);
//    add("Чистая рентаб"); //  отношение чистой прибыли компании к ее выручке.
    add(new Metric("Выручка", "млрд руб"));
//    add("P/S");
    add(CASH_AND_EQUIVALENTS);
    add(new Metric("Баланс стоимость", "млрд руб")); // или BV
    add(NET_ASSETS); // ЕСЛИ ЭТО БАНК, то у них нет такой строчки, зато есть активы - это одно и то же
//    add("BV/акцию");
    add(DEPT);
//    add(new Metric("Чистый долг", "млрд руб")); // = Долг - Наличность
//    add(PRICE_PER_BALANCE_VALUE);
    add(DIVIDEND); // если такого показателя нет, то можно считать, что он 0
//    add("Див.выплата"); // Дивиденд * Число акций
    add(DIVIDEND_EARNING); // если такого показателя нет, то можно считать, что он 0
//    add("Рентаб EBITDA"); //  рентабельность по показателю EBITDA, то есть отношение EBITDA/выручка.
    add(new Metric("EBITDA", "млрд руб"));
//    add("Долг/EBITDA");
//    add("EV/EBITDA");
//    add("Дивиденды/прибыль"); //  Див.выплата/Чистая прибыль
  }};

  @Override
  public void process(Set<ShareRatios> entities) {
    sharesAnalyser.analyze(entities);

    try (CSVWriter writer = new CSVWriter(new FileWriter("feedCrawler\\crawl_data\\shares.csv"))) {
      writer.writeNext(getHeaders(YEAR_NUMBER, chosenMetrics).toArray(new String[] {}));
      getAllRows(entities, YEAR_NUMBER, chosenMetrics)
          .stream()
          .filter(strings -> !strings.isEmpty())
          .forEach(row -> writer.writeNext(row.toArray(new String[] {})));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private List<List<String>> getAllRows(Set<ShareRatios> entities, int yearNumber, List<Metric> chosenMetrics) {
    return entities.stream()
        .flatMap(shareRatios -> getRowsFromSingleShareRatio(yearNumber, shareRatios, chosenMetrics).stream())
        .collect(Collectors.toList());
  }

  @VisibleForTesting
  public List<List<String>> getRowsFromSingleShareRatio(int yearNumber, ShareRatios shareRatios,
                                                        List<Metric> chosenMetrics) {
    List<String> years = getYears(shareRatios);
    log.debug("We have info for {} for years: {}", shareRatios.getId(), years);

    List<List<String>> metricValuesForYearSlices = new ArrayList<>();
    // each slice, like (2014, 2015, 2016) is converted to it's own row
    for (List<String> sliceYear : sliceList(years, yearNumber + 1)) {
      List<String> rowMetric = new ArrayList<>();
      rowMetric.add(shareRatios.getId());
      try {
        // currentYear < sliceYear.size() - 1 because last year for RESULT only
        for (int currentYear = 0; currentYear < sliceYear.size() - 1; currentYear++) {
          String year = sliceYear.get(currentYear);
          rowMetric.add(year);
          List<String> chosenMetricsValues = null;

          chosenMetricsValues = chosenMetrics.stream()
              .map(metric -> getValueForMetric(shareRatios.getRatios(), year, metric))
              .map(this::fmt)
              .collect(Collectors.toList());
          rowMetric.addAll(chosenMetricsValues);
        }
        // result
        rowMetric.add(sliceYear.get(sliceYear.size() - 1));
        rowMetric.add(String.valueOf(getProfit(shareRatios.getRatios(), sliceYear)));

        metricValuesForYearSlices.add(rowMetric);
      } catch (MetricNotFoundException exception) {
        log.warn("Metric not found for " + shareRatios.getId());
        log.warn(exception.getMessage());
        return Collections.emptyList();
      } catch (MetricValueForYearNotFound exception) {
        log.warn(
            "Metric value not found for " + shareRatios.getId() + " for slice " + sliceYear + ", passing this slice");
        log.warn(exception.getMessage());
        continue;
      }
    }
    return metricValuesForYearSlices;
  }

  public static List<String> getYears(ShareRatios shareRatios) {
    List<String> years = shareRatios.getRatios().values().stream()
        .flatMap(stringOptionalMap -> stringOptionalMap.keySet().stream())
        .distinct()
        .sorted()
        .collect(Collectors.toList());
    if (years.contains(LTM) && years.contains("2019")) {
      years.remove(LTM);
    }
    return years;
  }

  /**
   * Get result of slice Year - money diff = new share price - old share price +
   * all dividends that was earned since the second year (cause we are buying at the end
   * of the year and we not getting any divideds first year) divided by first year price
   *
   * @param ratios
   * @param sliceYear
   * @return
   */
  private int getProfit(Map<Metric, Map<String, Optional<Double>>> ratios, List<String> sliceYear) {
    log.debug("Counting result for sliceYear {}", sliceYear);
    double dividends = sliceYear.stream().skip(1)
        .mapToDouble(year -> getValueForMetric(ratios, year, DIVIDEND))
        .sum();
    log.debug("Dividend is {}", dividends);

    Double lastYearPrice = getValueForMetric(ratios, sliceYear.get(sliceYear.size() - 1), SHARE_PRICE);
    Double firstYearPrice = getValueForMetric(ratios, sliceYear.get(0), SHARE_PRICE);
    double shareDiff = lastYearPrice - firstYearPrice;
    log.debug("Share price diff is {}", shareDiff);
    return (int) Math.round((shareDiff + dividends)*100/firstYearPrice);
  }

  private Double getValueForMetric(Map<Metric, Map<String, Optional<Double>>> ratios, String year,
                                   Metric metric) {

    boolean metricIsDividend = metric.equals(DIVIDEND_EARNING) || metric.equals(DIVIDEND);
    if (metricIsDividend && (!ratios.containsKey(metric) || !ratios.get(metric).containsKey(year) || ratios.get(metric)
        .get(year).isEmpty())) {
      return 0.;
    }

    if (!ratios.containsKey(metric)) {
      if (metric.equals(NET_ASSETS) && ratios.containsKey(ASSETS)) {
        return ratios.get(ASSETS).get(year)
            .orElseThrow(() -> new MetricValueForYearNotFound(
                "There is no value for metric Активы/Чистые активы for year" + year));
      } else {
        throw new MetricNotFoundException("There is no metric " + metric.toString());
      }
    } else {
      return ratios.get(metric).get(year)
          .orElseThrow(() -> new MetricValueForYearNotFound(
              "There IS a metric " + metric.toString() + " but no value for year " + year));
    }
  }


  /**
   * makes from (2014, 2015, 2016, 2017, 2018) -> (2014, 2015, 2016), (2015, 2016, 2017), (2016, 2017, 2018)
   * if base = 3
   *
   * @param list
   * @param base
   * @param <T>
   * @return
   */
  @VisibleForTesting
  public <T> List<List<T>> sliceList(List<T> list, int base) {
    List<List<T>> out = new ArrayList<>();
    for (int step = 0; step + base <= list.size(); step++) {
      out.add(list.subList(step, step + base));
    }
    return out;
  }

  @VisibleForTesting
  public List<String> getHeaders(int yearNumber, List<Metric> chosenMetrics) {
    List<String> headers = new ArrayList<>();
    // id
    headers.add("id");
    // headers for metrics by years
    for (int year = -1 * yearNumber; year < 0; year++) {
      int finalYear = year;
      headers.add("n" + finalYear + " year");
      headers.addAll(chosenMetrics.stream()
          .map(metricName -> "n" + finalYear + " " + metricName.getName() + " " + metricName.getDimension())
          .collect(Collectors.toList()));
    }
    // result header
    headers.add("n year");
    headers.add("PROFIT");
    return headers;
  }

  public String fmt(double d) {
    if (d == (long) d) {
      return String.format("%d", (long) d);
    } else {
      return String.format("%s", d);
    }
  }
}
