package ru.diginetica.integration.feedCrawler.output.shares;

import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.ASSETS;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.CAPITALIZATION;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.CASH_AND_EQUIVALENTS;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.COMMON_SHARES_NUMBER;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.DEPT;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.DIVIDEND_EARNING;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.NET_ASSETS;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.NET_INCOME;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.PREMIUM_SHARES_NUMBER;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.PRICE_PER_BALANCE_VALUE;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.PRICE_PER_BOOK;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.PRICE_PER_EARNINGS;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.getYears;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.Metric;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.ShareRatios;
import ru.diginetica.integration.feedCrawler.output.PostProcessor;

@Slf4j
public class SharesAnalyzerPostProcessor implements PostProcessor<ShareRatios> {

  public static BiFunction<Double, Double, Double> decrementIsFlavoredFunction(double weight) {
    return (lastYearValue, currentYearValue) -> lastYearValue > currentYearValue ? weight : 0.;
  }

  public static BiFunction<Double, Double, Double> incrementIsFlavoredFunction(double weight) {
    return (lastYearValue, currentYearValue) -> lastYearValue < currentYearValue ? weight : 0.;
  }

  @Override
  public void process(Set<ShareRatios> entities) {
    entities.stream()
        .map(shareRatios -> Pair.of(shareRatios, getScore(shareRatios)))
        .sorted(Comparator.comparingDouble(Pair::getRight))
        .map(pair -> pair.getLeft().getId()
                     + " "
                     + pair.getLeft().getName()
                     + " Score="
                     + pair.getRight()
                     + " https://smart-lab.ru/q/"
                     + pair.getLeft().getId()
                     + "/f/y/MSFO/")
        .forEach(System.out::println);
  }

  /**
   * Calculate score for shareRatios
   *
   * @param shareRatios
   * @return
   */
  private double getScore(ShareRatios shareRatios) {
    if (shareRatios.getRatios().isEmpty()) {
      return 0.;
    }

    List<Pair<String, Map<Metric, Optional<Double>>>> metricsByYear = getYears(shareRatios).stream()
        .map(year -> Pair.of(year,
            Maps.transformValues(shareRatios.getRatios(), value -> value.getOrDefault(year, Optional.empty()))))
        .collect(Collectors.toList());

    try {
      double companyRedeemsCommonShares = getSingleIndicatorScoreDynamic(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(COMMON_SHARES_NUMBER, Optional.empty()),
          decrementIsFlavoredFunction(0.2));
      log.info("{} выкупает обыкновенные акции {}", shareRatios.getId(), companyRedeemsCommonShares);

      double companyRedeemsPremiumShares = getSingleIndicatorScoreDynamic(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(PREMIUM_SHARES_NUMBER, Optional.empty()),
          decrementIsFlavoredFunction(0.2));
      log.info("{} выкупает привелегированные акции {}", shareRatios.getId(), companyRedeemsPremiumShares);

      double companyDidNotHadLosses = getSingleIndicatorScoreStaticAllYears(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(NET_INCOME, Optional.empty()),
          value -> value < 0 ? -2. : 0.);
      log.info("{} У компании не было убытков {}", shareRatios.getId(), companyDidNotHadLosses);

      double companyRaisesCashAndEquivalents = getSingleIndicatorScoreDynamic(metricsByYear,
          this::getOrCashOrEquivalents,
          incrementIsFlavoredFunction(1));
      log.info("{} нарастила денежные средства и их эквиваленты {}", shareRatios.getId(),
          companyRaisesCashAndEquivalents);

      double companyDecreasesDept = getSingleIndicatorScoreDynamic(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(DEPT, Optional.empty()),
          decrementIsFlavoredFunction(1));
      log.info("{} Сократила долг {}", shareRatios.getId(), companyDecreasesDept);

      double companyCashPositionIncreased = getSingleIndicatorScoreDynamic(metricsByYear,
          metricOptionalMap -> getOrCashOrEquivalents(metricOptionalMap)
              .flatMap(cash -> metricOptionalMap.getOrDefault(DEPT, Optional.of(0.))
                  .map(dept -> cash - dept)),
          incrementIsFlavoredFunction(1));
      log.info("{} укрепила денежную позицию {}", shareRatios.getId(), companyCashPositionIncreased);

      double cashBiggerThanCapitalisation = getSingleIndicatorScoreStaticLastYear(metricsByYear,
          metricOptionalMap -> getOrCashOrEquivalents(metricOptionalMap)
              .flatMap(cash -> metricOptionalMap.getOrDefault(CAPITALIZATION, Optional.empty())
                  .map(capitalisation -> cash - capitalisation)),
          value -> value > 0 ? 2. : 0.);
      log.info("{} денежные средства и их эквиваленты больше капитализации {}", shareRatios.getId(),
          cashBiggerThanCapitalisation);

      double pricePerEarningsIsLow = getSingleIndicatorScoreStaticLastYear(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(PRICE_PER_EARNINGS, Optional.empty()),
          value -> value < 15 ? 1. : 0.);
      log.info("{} Показатель P/E < 15 {}", shareRatios.getId(), pricePerEarningsIsLow);

      double pricePerBalanceIsLow = getSingleIndicatorScoreStaticLastYear(metricsByYear,
          metricOptionalMap -> metricOptionalMap
              .getOrDefault(PRICE_PER_BOOK, metricOptionalMap.getOrDefault(PRICE_PER_BALANCE_VALUE, Optional.empty())),
          value -> value < 1.5 ? 1. : 0.);
      log.info("{} Показатель P/B < 1.5 {}", shareRatios.getId(), pricePerBalanceIsLow);

      double netAssetsGreaterThanDepth = getSingleIndicatorScoreStaticLastYear(metricsByYear,
          metricOptionalMap -> metricOptionalMap
              .getOrDefault(NET_ASSETS, metricOptionalMap.getOrDefault(ASSETS, Optional.empty()))
              .map(assets -> assets - metricOptionalMap.getOrDefault(DEPT, Optional.of(0.)).orElse(0.)),
          value -> value > 0 ? 4. : -4.);
      log.info("{} Активы превышают долги {}", shareRatios.getId(), netAssetsGreaterThanDepth);

      double growthRateBiggerThanPricePerEarnings = 0.;
      double growthRateMultiplier = getGrowthRateMultiplier(metricsByYear);
      if (growthRateMultiplier > 1) {
        growthRateBiggerThanPricePerEarnings = 2;
      } else if (growthRateMultiplier > 2) {
        growthRateBiggerThanPricePerEarnings = 3;
      }
      log.info("{} показатель роста относительно P/E {}", shareRatios.getId(), growthRateBiggerThanPricePerEarnings);

      double lastDividendYield = getSingleIndicatorScoreStaticLastYear(metricsByYear,
          metricOptionalMap -> metricOptionalMap.getOrDefault(DIVIDEND_EARNING, Optional.empty()),
          // todo: think about value
          value -> value > 10  ? 3. : value > 8 ? 2. : value > 0 ? 1. : 0);
      log.info("{} - последняя диведендная доходность", lastDividendYield);

      double all = companyRedeemsCommonShares +
                   companyRedeemsPremiumShares +
                   companyRaisesCashAndEquivalents +
                   companyDidNotHadLosses +
                   companyDecreasesDept +
                   companyCashPositionIncreased +
                   cashBiggerThanCapitalisation +
                   pricePerEarningsIsLow +
                   pricePerBalanceIsLow +
                   netAssetsGreaterThanDepth +
                   growthRateBiggerThanPricePerEarnings +
                   lastDividendYield +
                   0.;
      log.info("{} Score is {}", shareRatios.getName(), all);
      return all;
    } catch (YearNumberNotSequentialException exception) {
      log.error("Year number problem", exception);
      return 0.;
    }
  }

  private Optional<Double> getOrCashOrEquivalents(Map<Metric, Optional<Double>> metricOptionalMap) {
    return metricOptionalMap
        .getOrDefault(CASH_AND_EQUIVALENTS, metricOptionalMap.getOrDefault(ASSETS, Optional.empty()));
  }

  private double getGrowthRateMultiplier(List<Pair<String, Map<Metric, Optional<Double>>>> metricsByYear) {
    if (metricsByYear.size() < 2) {
      return 0.;
    }
    Map<Metric, Optional<Double>> lastYearMetrics = metricsByYear.get(metricsByYear.size() - 1).getRight();
    Optional<Double> lastYearNetIncome = lastYearMetrics.getOrDefault(NET_INCOME, Optional.empty());
    Optional<Double> prelastYearNetIncome =
        metricsByYear.get(metricsByYear.size() - 2).getRight().getOrDefault(NET_INCOME, Optional.empty());
    if (!lastYearNetIncome.isPresent() || !prelastYearNetIncome.isPresent()) {
      return 0.;
    }

    double growthTempo = lastYearNetIncome.get() / prelastYearNetIncome.get() * 100 - 100;

    return lastYearMetrics.getOrDefault(PRICE_PER_EARNINGS, Optional.empty())
        .map(pricePerEarnings -> (growthTempo
                                  + lastYearMetrics.getOrDefault(DIVIDEND_EARNING, Optional.empty()).orElse(0.))
                                 / pricePerEarnings)
        .orElse(0.);
  }

  @VisibleForTesting
  double getSingleIndicatorScoreStaticLastYear(List<Pair<String, Map<Metric, Optional<Double>>>> metricsByYear,
                                               Function<Map<Metric, Optional<Double>>, Optional<Double>> fetchIndicator,
                                               Function<Double, Double> countWeight) {
    return fetchIndicator.apply(metricsByYear.get(metricsByYear.size() - 1).getRight())
        .map(countWeight::apply)
        .orElse(0.);
  }

  @VisibleForTesting
  double getSingleIndicatorScoreStaticAllYears(List<Pair<String, Map<Metric, Optional<Double>>>> metricsByYear,
                                               Function<Map<Metric, Optional<Double>>, Optional<Double>> fetchIndicator,
                                               Function<Double, Double> countWeight) {
    return metricsByYear.stream()
        .mapToDouble(pair -> fetchIndicator.apply(pair.getRight())
            .map(countWeight::apply)
            .orElse(0.))
        .sum();
  }

  @VisibleForTesting
  double getSingleIndicatorScoreDynamic(List<Pair<String, Map<Metric, Optional<Double>>>> metricsByYear,
                                        Function<Map<Metric, Optional<Double>>, Optional<Double>> fetchIndicator,
                                        BiFunction<Double, Double, Double> countWeight) {
    AtomicReference<Double> allYearsWeight = new AtomicReference<>(0.);
    metricsByYear.stream()
        .reduce((yearMetricValue1, yearMetricValue2) -> {
          String year1 = yearMetricValue1.getLeft();
          String year2 = yearMetricValue2.getLeft();
          if (NumberUtils.isCreatable(year1) && NumberUtils.isCreatable(year2) &&
              Integer.parseInt(year2) - Integer.parseInt(year1) != 1) {
            throw new YearNumberNotSequentialException("Year number is not sequential");
          }
          Optional<Double> value1 = fetchIndicator.apply(yearMetricValue1.getRight());
          Optional<Double> value2 = fetchIndicator.apply(yearMetricValue2.getRight());

          if (value1.isPresent() && value2.isPresent()) {
            allYearsWeight.updateAndGet(v -> v + countWeight.apply(value1.get(), value2.get()));
          }

          return yearMetricValue2;
        });
    return allYearsWeight.get();
  }

  class YearNumberNotSequentialException extends RuntimeException {
    public YearNumberNotSequentialException(String message) {
      super(message);
    }
  }

}
