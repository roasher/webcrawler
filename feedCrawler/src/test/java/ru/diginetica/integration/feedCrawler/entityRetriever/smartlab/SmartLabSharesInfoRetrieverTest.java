package ru.diginetica.integration.feedCrawler.entityRetriever.smartlab;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.junit.Test;

public class SmartLabSharesInfoRetrieverTest {

  private SmartLabSharesInfoRetriever smartLabSharesInfoRetriever =
      new SmartLabSharesInfoRetriever(Pattern.compile("https://smart-lab.ru/q/(.+)/f/y/"));

  @Test
  @SneakyThrows
  public void retriveSberTable() {
    Optional<ShareRatios> entityFromPage = smartLabSharesInfoRetriever.getEntityFromPage(Jsoup.parse(new File(
            "src/test/resources/ru/diginetica/integration/feedCrawler/entityRetriever/smartlab/SBER.html"),
        Charset.defaultCharset().name()), new URL("https://smart-lab.ru/q/SBER/f/y/"));

    assertTrue(entityFromPage.isPresent());
    assertThat(entityFromPage.get().getId(), is("SBER"));
    assertThat(entityFromPage.get().getName(), is("Сбербанк (SBER) фундаментальный анализ акций"));
    assertThat(entityFromPage.get().getRatios().size(), is(49));

    assertThat(entityFromPage.get().getRatios().get(new Metric("P/E", "")), is(Map.of(
        "2014", Optional.of(4.21),
        "2015", Optional.of(10.2),
        "2016", Optional.of(7.14),
        "2017", Optional.of(6.74),
        "2018", Optional.of(6.79),
        "LTM", Optional.of(6.39)
    )));

    assertThat(entityFromPage.get().getRatios().get(new Metric("Капитализация", "млрд руб")), is(Map.of(
        "2014", Optional.of(1223.),
        "2015", Optional.of(2263.),
        "2016", Optional.of(3870.),
        "2017", Optional.of(5046.),
        "2018", Optional.of(5645.),
        "LTM", Optional.of(5645.)
    )));

    assertThat(entityFromPage.get().getRatios().get(new Metric("Депозиты", "млрд руб")), is(Map.of(
        "2014", Optional.empty(),
        "2015", Optional.empty(),
        "2016", Optional.of(18685.),
        "2017", Optional.of(19814.),
        "2018", Optional.of(20897.),
        "LTM", Optional.of(22318.)
    )));
  }

  @Test
  @SneakyThrows
  public void retriveNornikelTable() {
    Optional<ShareRatios> entityFromPage = smartLabSharesInfoRetriever.getEntityFromPage(Jsoup.parse(new File(
            "src/test/resources/ru/diginetica/integration/feedCrawler/entityRetriever/smartlab/GMKN.html"),
        Charset.defaultCharset().name()), new URL("https://smart-lab.ru/q/GMKN/f/y/"));

    assertTrue(entityFromPage.isPresent());
    assertThat(entityFromPage.get().getId(), is("GMKN"));
    assertThat(entityFromPage.get().getName(), is("ГМК Норникель (GMKN) фундаментальный анализ акций"));
    assertThat(entityFromPage.get().getRatios().size(), is(43));

    assertThat(entityFromPage.get().getRatios().get(new Metric("P/E", "")), is(Map.of(
        "2015", Optional.of(9.91),
        "2016", Optional.of(10.7),
        "2017", Optional.of(13.4),
        "2018", Optional.of(10.9),
        "2019", Optional.empty(),
        "LTM", Optional.of(12.6)
    )));

    assertThat(entityFromPage.get().getRatios().get(new Metric("Капитализация", "млрд руб")), is(Map.of(
        "2015", Optional.of(1448.),
        "2016", Optional.of(1602.),
        "2017", Optional.of(1717.),
        "2018", Optional.of(2063.),
        "2019", Optional.of(3516.),
        "LTM", Optional.of(3516.)
    )));

    assertThat(entityFromPage.get().getRatios().get(new Metric("Расходы/чел/год", "тыс.р")), is(Map.of(
        "2015", Optional.of(1095.),
        "2016", Optional.of(929.8),
        "2017", Optional.empty(),
        "2018", Optional.empty(),
        "2019", Optional.empty(),
        "LTM", Optional.empty()
    )));
  }

}