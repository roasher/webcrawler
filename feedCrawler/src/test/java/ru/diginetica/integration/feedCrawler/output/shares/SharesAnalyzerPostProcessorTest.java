package ru.diginetica.integration.feedCrawler.output.shares;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesAnalyzerPostProcessor.decrementIsFlavoredFunction;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.COMMON_SHARES_NUMBER;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

public class SharesAnalyzerPostProcessorTest {

  private SharesAnalyzerPostProcessor sharesAnalyzerPostProcessor = new SharesAnalyzerPostProcessor();

  @Test
  public void redeemCommonSharesWeight() {
    double singleIndicatorScoreDynamic = sharesAnalyzerPostProcessor.getSingleIndicatorScoreDynamic(List.of(
        Pair.of("2015", Map.of(COMMON_SHARES_NUMBER, Optional.of(1000.))),
        Pair.of("2016", Map.of(COMMON_SHARES_NUMBER, Optional.of(500.))),
        Pair.of("2017", Map.of(COMMON_SHARES_NUMBER, Optional.empty())),
        Pair.of("2018", Map.of(COMMON_SHARES_NUMBER, Optional.of(1000.))),
        Pair.of("2019", Map.of(COMMON_SHARES_NUMBER, Optional.of(500.))),
        Pair.of("2020", Map.of(COMMON_SHARES_NUMBER, Optional.of(400.)))
        ), metricOptionalMap -> metricOptionalMap.getOrDefault(COMMON_SHARES_NUMBER, Optional.empty()),
        decrementIsFlavoredFunction(1.));
    assertThat(singleIndicatorScoreDynamic, is(3.));
  }
}