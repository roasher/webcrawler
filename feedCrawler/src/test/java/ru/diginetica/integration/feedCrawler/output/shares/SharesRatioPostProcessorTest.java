package ru.diginetica.integration.feedCrawler.output.shares;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static ru.diginetica.integration.feedCrawler.output.shares.SharesRatioPostProcessor.ASSETS;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.Metric;
import ru.diginetica.integration.feedCrawler.entityRetriever.smartlab.ShareRatios;

@RunWith(MockitoJUnitRunner.class)
public class SharesRatioPostProcessorTest {

  @InjectMocks
  private SharesRatioPostProcessor sharesRatioPostProcessor;

  @Mock
  private SharesAnalyser sharesAnalyser;

  @Test
  public void checkSlice() {
    assertThat(sharesRatioPostProcessor.sliceList(List.of("2014", "2015", "2016", "2017", "2018"), 3), is(List.of(
        List.of("2014", "2015", "2016"),
        List.of("2015", "2016", "2017"),
        List.of("2016", "2017", "2018")
    )));
  }

  @Test
  public void getRightHeaders() {
    List<Metric> chosenMetrics = List.of(new Metric("1", "dim"), new Metric("2", "dim"));
    assertThat(sharesRatioPostProcessor.getHeaders(2, chosenMetrics), is(
        List.of("id", "n-2 year", "n-2 1 dim", "n-2 2 dim", "n-1 year", "n-1 1 dim", "n-1 2 dim", "n year", "PROFIT")
    ));
  }

  @Test
  public void getRowsFromSingleShareRatio() {
    List<Metric> chosenMetrics = List.of(
        SharesRatioPostProcessor.SHARE_PRICE,
        SharesRatioPostProcessor.DIVIDEND,
        new Metric("Выручка", "млрд руб")
    );
    List<List<String>> rowsFromSingleShareRatio =
        sharesRatioPostProcessor.getRowsFromSingleShareRatio(2, new ShareRatios("SBER", null, Map.of(
            chosenMetrics.get(0),
            Map.of("2014", Optional.of(100.), "2015", Optional.of(300.), "2016", Optional.of(400.), "2017",
                Optional.of(600.)),
            chosenMetrics.get(1),
            Map.of("2014", Optional.of(1.), "2015", Optional.of(3.), "2016", Optional.of(4.), "2017",
                Optional.of(6.)),
            chosenMetrics.get(2),
            Map.of("2014", Optional.of(1111.), "2015", Optional.of(2222.), "2016", Optional.of(3333.), "2017",
                Optional.of(4444.))
        )), chosenMetrics);

    assertThat(rowsFromSingleShareRatio, is(List.of(
        List.of("SBER", "2014", "100", "1", "1111", "2015", "300", "3", "2222", "2016", "307"),
        List.of("SBER", "2015", "300", "3", "2222", "2016", "400", "4", "3333", "2017", "103")
    )));
  }

  @Test
  public void getRowsDividentIsZeroIfNone() {
    List<Metric> chosenMetrics = List.of(
        SharesRatioPostProcessor.DIVIDEND_EARNING,
        SharesRatioPostProcessor.DIVIDEND,
        SharesRatioPostProcessor.SHARE_PRICE
    );
    List<List<String>> rowsFromSingleShareRatio =
        sharesRatioPostProcessor.getRowsFromSingleShareRatio(1, new ShareRatios("SBER", null, Map.of(
            chosenMetrics.get(2),
            Map.of("2014", Optional.of(100.), "2015", Optional.of(300.), "2016", Optional.of(400.), "2017",
                Optional.of(600.))
        )), chosenMetrics);

    assertThat(rowsFromSingleShareRatio, is(List.of(
        List.of("SBER", "2014", "0", "0", "100", "2015", "200"),
        List.of("SBER", "2015", "0", "0", "300", "2016", "33"),
        List.of("SBER", "2016", "0", "0", "400", "2017", "50")
    )));
  }

  @Test
  public void useActiveIfThereIsNoCleanActive() {
    List<Metric> chosenMetrics = List.of(
        SharesRatioPostProcessor.NET_ASSETS,
        SharesRatioPostProcessor.DIVIDEND,
        SharesRatioPostProcessor.SHARE_PRICE
    );
    List<List<String>> rowsFromSingleShareRatio =
        sharesRatioPostProcessor.getRowsFromSingleShareRatio(2, new ShareRatios("SBER", null, Map.of(
            ASSETS,
            Map.of("2014", Optional.of(10.), "2015", Optional.of(20.), "2016", Optional.of(30.), "2017",
                Optional.of(40.)),
            chosenMetrics.get(2),
            Map.of("2014", Optional.of(100.), "2015", Optional.of(300.), "2016", Optional.of(400.), "2017",
                Optional.of(600.))
        )), chosenMetrics);

    assertThat(rowsFromSingleShareRatio, is(List.of(
        List.of("SBER", "2014", "10", "0", "100",  "2015", "20", "0", "300", "2016", "300"),
        List.of("SBER", "2015", "20", "0", "300", "2016", "30", "0", "400", "2017", "100")
    )));
  }
}