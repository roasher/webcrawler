package ru.diginetica.integration.strategyCrawler.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.common.base.Splitter;

@Component("PropertySplitter")
public class PropertySplitter {

  /**
   * Example: one.example.property = KEY1:VALUE1.1,VALUE1.2;KEY2:VALUE2.1,VALUE2.2
   */
  public Map<String, List<String>> mapOfList(String property) {
    Map<String, String> map = this.map(property, ";");

    Map<String, List<String>> mapOfList = new HashMap<>();
    for (Map.Entry<String, String> entry : map.entrySet()) {
      mapOfList.put(entry.getKey(), this.list(entry.getValue()));
    }

    return mapOfList;
  }

  /**
   * Example: one.example.property = VALUE1,VALUE2,VALUE3,VALUE4
   */
  public List<String> list(String property) {
    return this.list(property, ",");
  }

  private List<String> list(String property, String splitter) {
    return Splitter.on(splitter).omitEmptyStrings().trimResults().splitToList(property);
  }

  private Map<String, String> map(String property, String splitter) {
    return Splitter.on(splitter).omitEmptyStrings().trimResults().withKeyValueSeparator(":").split(property);
  }

}