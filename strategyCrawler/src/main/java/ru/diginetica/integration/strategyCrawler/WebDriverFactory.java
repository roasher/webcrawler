package ru.diginetica.integration.strategyCrawler;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class WebDriverFactory implements DisposableBean {

  private WebDriver jsDriver;
  private WebDriver noJsDriver;

  public WebDriver getWebDriver(boolean useJs) {
    if (useJs) {
      if (jsDriver == null) {
        ChromeOptions op = new ChromeOptions();
        op.addExtensions(new File("/home/pyurkin/tools/googleChromeUserDataForCrawling/Block-image_v1.1.crx"));
        jsDriver = new ChromeDriver(op);
//        jsDriver = new FirefoxDriver();
      }
      return jsDriver;
    } else {
      if (noJsDriver == null) {
        HtmlUnitDriver driver = new HtmlUnitDriver();
        driver.setJavascriptEnabled(false);
        noJsDriver = driver;
      }
      return noJsDriver;
    }
  }

  @Override
  public void destroy() throws Exception {
    if (jsDriver != null) jsDriver.close();
    if (noJsDriver != null) noJsDriver.close();
  }
}
