package ru.diginetica.integration.strategyCrawler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.diginetica.integration.strategyCrawler.crawler.JourneyCrawler;

/**
 * Created by pyurkin on 06.06.16.
 */
public class JourneyController {

  public static void main(String... arg) throws IOException {
    System.setProperty("webdriver.chrome.driver", "/home/pyurkin/tools/webBrowserDrivers/chrome/chromedriver");
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-configuration.xml");
    JourneyCrawler journeyCrawler = applicationContext.getBean(JourneyCrawler.class);
    //Map<CategoryUrl, Map<Product, Map<CrawlSelector, Set<Recommendations>>>>
    Map<String, Map<String, Map<String, Set<String>>>> result = journeyCrawler.crawl();
    writeRecomendations(result, "strategyCrawler/crawl_data/" + System.getProperty("site") + "-output.txt");
  }

  private static void writeRecomendations(
    Map<String, Map<String, Map<String, Set<String>>>> result, String filePath) throws FileNotFoundException {
    try (PrintWriter writer = new PrintWriter(filePath)) {
      result.forEach((categoryUrl, productMap) -> {
        writer.println("Crawled from category: " + categoryUrl);
        productMap.forEach((product, locatorRecs) -> {
          writer.println("\tCrawled for product selector: " + product);
          locatorRecs.forEach((locator, recommendations) -> {
            writer.println("\t\tCrawled from locator selector: " + locator + " - " + recommendations.size() + " "
              + "recommendations:");
            recommendations.forEach(recommendation -> {
              writer.println("\t\t\t" + recommendation);
            });
          });
        });
      });
      writer.flush();
    }
  }
}
