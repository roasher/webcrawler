package ru.diginetica.integration.strategyCrawler.config;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by pyurkin on 06.06.16.
 */
@Component
@PropertySource("classpath:config/${site}-journey.properties")
public class JourneyConfig {

  @Value("#{PropertySplitter.mapOfList('${journeyMap}')}")
  private Map<String,List<String>> journeyMap;
  @Value("${next}")
  private String next;
  @Value("${isPageLoadedScript:true}")
  private String isPageLoadedScript;
  @Value("${crawlSelector}")
  private String[] crawlSelector;
  @Value("${sleepBeforeFindElementOnPage:0}")
  private int sleepBeforeFindElementOnPage;
  @Value("${jQuerySelector:$}")
  private String jQuerySelector;

  public Map<String, List<String>> getJourneyMap() {
    return journeyMap;
  }

  public String getNext() {
    return next;
  }

  public String[] getCrawlSelector() {
    return crawlSelector;
  }

  public String getIsPageLoadedScript() {
    return isPageLoadedScript;
  }

  public int getSleepBeforeFindElementOnPage() {
    return sleepBeforeFindElementOnPage;
  }

  public String getjQuerySelector() {
    return jQuerySelector;
  }
}
