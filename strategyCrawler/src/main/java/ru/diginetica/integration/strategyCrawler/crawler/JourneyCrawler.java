package ru.diginetica.integration.strategyCrawler.crawler;

import com.google.common.base.Function;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.diginetica.integration.strategyCrawler.WebDriverFactory;
import ru.diginetica.integration.strategyCrawler.config.JourneyConfig;

/**
 * Created by pyurkin on 06.06.16.
 */
@Component
@Slf4j
public class JourneyCrawler {

  @Autowired
  private WebDriverFactory webDriverFactory;
  @Autowired
  private JourneyConfig journeyConfig;

  /**
   * returns Map<CategoryUrl, Map<Product, Map<CrawlSelector, Set<Recommendations>>>>
   * @return
   */
  public Map<String, Map<String, Map<String, Set<String>>>> crawl() {
    WebDriver webDriver = webDriverFactory.getWebDriver(true);
    webDriver.manage().deleteAllCookies();
    WebDriverWait wait = new WebDriverWait(webDriver, 10);

    Map<String, Map<String, Map<String, Set<String>>>> out = new HashMap<>();
    journeyConfig.getJourneyMap().forEach((categoryUrl, products) -> {
      Map<String, Map<String, Set<String>>> productRecs = new HashMap<>();
      out.put(categoryUrl, productRecs);
      products.forEach(product -> {
        webDriver.get("http://" + categoryUrl);
        productRecs.put(product, searchForProductRecomendations(webDriver, wait, product));
      });
    });
    return out;
  }

  /**
   * Assuming webDriver already on needed category page with paging
   * @param webDriver
   * @param wait
   * @param productUrl
   * @return
   */
  private Map<String, Set<String>> searchForProductRecomendations(WebDriver webDriver, WebDriverWait wait, String productUrl) {
    Map<String, Set<String>> result = new HashMap<>();
    while (true) {
      sleepFor(journeyConfig.getSleepBeforeFindElementOnPage());
      log.info("Searching product on page: {}", webDriver.getCurrentUrl());
      Optional<WebElement> product = findElementOnPage(webDriver, By.cssSelector(productUrl));
      if (product.isPresent()) {
        wait.until(ExpectedConditions.visibilityOf(product.get()));
        clickOnElement(webDriver, product.get());
        log.info("Product found {}", webDriver.getCurrentUrl());
        waitForRecommendationsToAppear(wait);
        for (String crawlSelector : journeyConfig.getCrawlSelector()) {
          result.put(crawlSelector, webDriver.findElements(By.cssSelector(crawlSelector)).stream().map(webElement -> webElement
            .getAttribute("href"))
            .collect(Collectors.toSet()));
        }
        return result;
      } else {
        Optional<WebElement> nextButton = findElementOnPage(webDriver, By.cssSelector(journeyConfig.getNext()));
        if (nextButton.isPresent()) {
          clickOnElement(webDriver, nextButton.get());
          waitForPageLoad(wait);
        } else {
          log.info("Product not found");
          return Collections.emptyMap();
        }
      }
    }
  }

  private void clickOnElement(WebDriver webDriver, WebElement product) {
    JavascriptExecutor js = (JavascriptExecutor) webDriver;
    js.executeScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, "
      + "0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", product);
  }

  private Optional<WebElement> findElementOnPage(WebDriver webDriver, By by) {
    try {
      return Optional.of(webDriver.findElement(by));
    } catch (NoSuchElementException exception) {
      return Optional.empty();
    }
  }

  private void waitForRecommendationsToAppear(WebDriverWait wait) {
    Function<WebDriver, Boolean> someRecomendationAppeared = webDriver -> {
      long visibleCount = Arrays.stream(journeyConfig.getCrawlSelector())
        .filter(crawlArea -> Boolean.valueOf(
          ((JavascriptExecutor) webDriver).executeScript("return " + journeyConfig.getjQuerySelector() + "('" + crawlArea + "')"
            + ".length > 0")
            .toString()))
        .count();
      return visibleCount > 0;
    };
    wait.until(someRecomendationAppeared);
  }

  private void waitForPageLoad(WebDriverWait wait) {
    wait.until((Function<WebDriver, Boolean>) webDriver -> Boolean.valueOf(
      ((JavascriptExecutor) webDriver).executeScript("return " + journeyConfig.getIsPageLoadedScript()).toString()
    ));
  }

  private void sleepFor(long millis) {
    if (millis == 0) return;
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      throw new RuntimeException("Crawler thread sleep was interrupted");
    }
  }
}
